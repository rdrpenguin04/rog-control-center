mod config;
mod error;
mod signal_handlers;

use nix::{sys::stat, unistd};
use rog_dbus::{DbusProxies, Signals};
use std::{
    fs::{remove_dir_all, OpenOptions},
    io::{Read, Write},
    process::exit,
    rc::Rc,
    sync::{Arc, Mutex},
    thread::{sleep, spawn},
    time::Duration,
};
use tempfile::TempDir;
//use log::{error, info, warn};

use crate::config::Config;

slint::include_modules!();

const SHOWING_GUI: u8 = 1;
const SHOW_GUI: u8 = 2;

/// Either exit the process, or return with a refreshed tmp-dir
fn on_tmp_dir_exists() -> Result<TempDir, std::io::Error> {
    let mut buf = [0u8; 4];
    let path = std::env::temp_dir().join("rog-gui");

    let mut ipc_file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(false)
        .open(path.join("ipc.pipe"))?;

    // If the app is running this ends up stacked on top of SHOWING_GUI
    ipc_file.write_all(&[SHOW_GUI])?;
    // tiny sleep to give the app a chance to respond
    sleep(Duration::from_millis(10));
    ipc_file.read(&mut buf).ok();

    // First entry is the actual state
    if buf[0] == SHOWING_GUI {
        ipc_file.write_all(&[SHOWING_GUI])?; // Store state again as we drained the fifo
        exit(0);
    } else if buf[0] == SHOW_GUI {
        remove_dir_all(&path)?;
        return tempfile::Builder::new()
            .prefix("rog-gui")
            .rand_bytes(0)
            .tempdir();
    }
    exit(-1);
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Startup
    let mut buf = [0];
    let mut config = Config::load()?;

    // tmp-dir must live to the end of program life
    let tmp_dir = match tempfile::Builder::new()
        .prefix("rog-gui")
        .rand_bytes(0)
        .tempdir()
    {
        Ok(tmp) => tmp,
        Err(_) => on_tmp_dir_exists()?,
    };

    let fifo_path = tmp_dir.path().join("ipc.pipe");

    unistd::mkfifo(&fifo_path, stat::Mode::S_IRWXU)?;

    let mut ipc_file = OpenOptions::new()
        .read(true)
        .write(true)
        .truncate(true)
        .open(&fifo_path)?;
    //

    let app = App::new();
    let shared_app = Rc::new(app);

    let (proxies, _) = DbusProxies::new()?;
    let shared_proxies = Rc::new(proxies);

    // Check for signals
    let (proxies, conn) = DbusProxies::new()?;
    let signals = Arc::new(Mutex::new(Signals::new(&proxies)?));
    spawn(move || {
        // recv must be created in the thread because of lifetime issues
        let recv = proxies.setup_recv(conn);
        loop {
            // next_signal is blocking
            if let Err(err) = recv.next_signal() {
                println!("{}", err);
            }
        }
    });

    // TODO: option to enable or disable the signal/notif thread
    signal_handlers::start_signal_watch(shared_app.as_weak(), signals);

    if config.startup_in_background {
        config.run_in_background = true;
        config.save().ok();
    }
    let mut should_show_gui = !config.startup_in_background;
    shared_app.set_background_run_checked(config.run_in_background);
    shared_app.set_background_start_checked(config.startup_in_background);

    let config = Arc::new(Mutex::new(config));
    loop {
        if should_show_gui {
            // Do app setup each time
            ipc_file.write_all(&[SHOWING_GUI])?;
            setup_system_page(shared_app.clone(), shared_proxies.clone(), config.clone())?;
            shared_app.clone().run();
            // if `run` exits then
            should_show_gui = false;
        }

        if !shared_app.clone().get_background_run_checked() {
            break Ok(());
        }

        // Loop is blocked here until a single byte is read
        if ipc_file.read_exact(&mut buf).is_ok() && buf[0] == SHOW_GUI {
            should_show_gui = true;
        }
    }
}

fn setup_system_page(
    app: Rc<App>,
    shared_dbus: Rc<DbusProxies<'static>>,
    config: Arc<Mutex<Config>>,
) -> Result<(), zbus::fdo::Error> {
    let supported = shared_dbus.supported().get_supported_functions()?;

    const UNSUPPORTED: &str = "This feature is unsupported by your hardware";
    let mut gui_support_data = app.get_data_page_system();

    // TODO: requires kernel patch completion and add to asusctl
    app.set_panelod_checked(false);
    app.set_panelod_enabled(false);
    gui_support_data.panelod.sub = UNSUPPORTED.into();

    // SOUND
    // TODO: make this or part of it a macro
    let weak_app = app.as_weak();
    let sdbus = shared_dbus.clone();
    app.on_sound_toggled(move |state| {
        sdbus.rog_bios().set_post_sound(state).ok(); // TODO: Change all these to warnings
        weak_app.unwrap().set_sound_checked(state);
    });
    let tmp = shared_dbus.rog_bios().get_post_sound()?;
    app.set_sound_checked(tmp == 1);
    app.set_sound_enabled(supported.rog_bios_ctrl.post_sound_toggle);
    if !supported.rog_bios_ctrl.post_sound_toggle {
        gui_support_data.sound.sub = UNSUPPORTED.into();
    }

    // DGPU/GSYNC
    let weak_app = app.as_weak();
    let sdbus = shared_dbus.clone();
    app.on_gsync_toggled(move |state| {
        sdbus.rog_bios().set_dedicated_gfx(state).ok();
        weak_app.unwrap().set_gsync_checked(state);
    });
    let tmp = shared_dbus.rog_bios().get_dedicated_gfx()?;
    app.set_gsync_checked(tmp == 1);
    app.set_gsync_enabled(supported.rog_bios_ctrl.dedicated_gfx_toggle);
    if !supported.rog_bios_ctrl.dedicated_gfx_toggle {
        gui_support_data.gsync.sub = UNSUPPORTED.into();
    }

    // KEYBOARD
    let weak_app = app.as_weak();
    let sdbus = shared_dbus.clone();
    app.on_leds_enable_toggled(move |state| {
        sdbus.led().set_awake_enabled(state).ok();
        weak_app.unwrap().set_leds_checked(state);
    });
    let tmp = shared_dbus.led().awake_enabled()?;
    app.set_leds_checked(tmp);
    app.set_leds_enabled(supported.keyboard_led.brightness_set);
    if !supported.keyboard_led.brightness_set {
        gui_support_data.leds_enable.sub = UNSUPPORTED.into();
    }

    let weak_app = app.as_weak();
    let sdbus = shared_dbus.clone();
    app.on_leds_sleep_enable_toggled(move |state| {
        sdbus.led().set_sleep_enabled(state).ok();
        weak_app.unwrap().set_leds_sleep_checked(state);
    });
    let tmp = shared_dbus.led().sleep_enabled()?;
    app.set_leds_sleep_checked(tmp);
    app.set_leds_sleep_enabled(supported.keyboard_led.brightness_set);
    if !supported.keyboard_led.brightness_set {
        gui_support_data.leds_sleep.sub = UNSUPPORTED.into();
    }

    // ANIME
    let weak_app = app.as_weak();
    let sdbus = shared_dbus.clone();
    app.on_anime_enable_toggled(move |state| {
        sdbus.anime().set_on_off(state).ok();
        weak_app.unwrap().set_anime_checked(state);
    });
    let tmp = shared_dbus.anime().awake_enabled()?;
    app.set_anime_checked(tmp);
    app.set_anime_enabled(supported.anime_ctrl.0);
    if !supported.anime_ctrl.0 {
        gui_support_data.anime_enable.sub = UNSUPPORTED.into();
    }

    let weak_app = app.as_weak();
    let sdbus = shared_dbus.clone();
    app.on_anime_boot_enable_toggled(move |state| {
        if sdbus.anime().set_boot_on_off(state).is_err() {
            weak_app.unwrap().set_anime_boot_checked(!state);
        }
    });
    let tmp = shared_dbus.anime().boot_enabled()?;
    app.set_anime_boot_checked(tmp);
    app.set_anime_boot_enabled(supported.anime_ctrl.0);
    if !supported.anime_ctrl.0 {
        gui_support_data.anime_boot.sub = UNSUPPORTED.into();
    }

    // User config
    let cfg = config.clone();
    app.on_background_run_toggled(move |state| {
        if let Ok(mut lock) = cfg.try_lock() {
            lock.run_in_background = state;
            lock.save().ok();
        }
    });

    let weak_app = app.as_weak();
    app.on_background_start_toggled(move |state| {
        if let Ok(mut lock) = config.try_lock() {
            lock.startup_in_background = state;
            if lock.startup_in_background {
                lock.run_in_background = true;
                weak_app.unwrap().set_background_run_checked(true);
            }
            lock.save().ok();
        }
    });

    app.set_data_page_system(gui_support_data);
    Ok(())
}
